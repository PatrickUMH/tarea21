struct auto
{
  int nPasajeros;
  int capTanque;
  float velocidadMax;
  int  nVelocidades;
};
struct computadoras
{
  int memoriaRAM;
  int discoDuro;
  float velProcesador;
  int nNucleos;
  char tarjetaGrafica;
  char tipoRAM;
};
struct alumno
{
  int numClases;
  char Nombre[60];
  char Carrera[25];
  char numCuenta[10];
  char plantel[20];
  float promedio;
};
struct equipoDeFutbol
{
  char manager[60];
  char MVP[60];
  int copas;
  int partidos;
  int clasificaciones;
};
struct mascota
{
  char nombre[20];
  char raza[30];
  float peso;
  float estatura;
  int edad;
};
struct videoJuegos
{
  char nombre[60];
  char nombreDeDesarrolladora[60];
  float costo;
  char fechaDeSalida;
  char genero[20];
  int numJugadoresLocal;
  int numJugadoresInterConexion;
  int numJugadoresOnline;
  float peso;
};
struct superHeroe
{
  char nombreDeHeroe[30];
  char nombre[60];
  char nombreCreador[60];
  int numComics;
  int numPeliculas;
};
struct planetas
{
  char nombre[30];
  float distanciaATierra;
  float masa;
};
struct personajeDeJuegoFavorito
{
  char nombreDeDesarrolladora[60];
  char nombreDelJuego[60];
  char nombrePersonaje[60];
  int numHabilidades;
  char genero;
  char raza;
  int numAliados;
  char nombreAliado[60];
  float estatura;
  char edad;//puede ser desconocida, por es el char
};
struct comic
{
  char nombreCreador[60];
  char nombre[60];
  int numComics;
  int añoDeCreacion;
  int numPersonajes;
  char personajePrincipal[60];
};
